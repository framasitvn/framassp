var siteUrl = "http://portal.framas.com/sites/kaizen";

// GET LIST BANK
function get_List_Kaizen_Submission() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Kaizen_Submission')/items?$top=1000&$filter=kaizen_status eq 'Approved' or kaizen_status eq 'Finish'",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

function get_Report_List_Kaizen_Submission() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Kaizen_Submission')/items?$top=1000&$filter=(kaizen_status eq 'Approved' or kaizen_status eq 'Finish') and Remark ne 0",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST BANK
function get_List_Bank_Testing() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Bank_Testing')/items?$top=1000&$filter=kaizen_status eq 'Approved'",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST USER
function get_User() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/SiteUsers",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST LOCATIONS
function get_Locations() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Location')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST AREA
function get_Areas() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Area')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST DEPARTMENT
function get_Department() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Department')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST EQUIPMENTS
function get_Equipments() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Equipment')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST WORKPIECES
function get_Workpieces() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Workpiece')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST IMPLEMENTATION DEPARTMENTS
function get_Implementation_Departments() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Implementation_Department')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST OUTCOMES
function get_Outcomes() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Outcome')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST TYPE OF KAIZEN
function get_Kaizen_Type() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Type_Kaizen')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}
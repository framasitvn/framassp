// GET USER NAME & REMARK DATA
// var arr_List_Bank = get_List_JSON("list_kaizen_bank")
// var arr_users = get_List_JSON("list_users")

// $(".carousel").slick({
//   infinite: true,
//   slidesToShow: 1,
//   slidesToScroll: 1,
//   arrows: true,
//   //   fade: true,
//   autoplay: true,
//   autoplaySpeed: 3000,
//   prevArrow: '<button class="slide-arrow prev-arrow"></button>',
//   nextArrow: '<button class="slide-arrow next-arrow"></button>'
// });

SP.SOD.executeFunc("sp.js", "SP.ClientContext", function() {
  // var arr_List_Bank = get_List_Kaizen_Submission();
  var arr_List_Bank = get_Report_List_Kaizen_Submission()
  var arr_users = get_User();

  console.log(arr_List_Bank)

  var arr_user_name = new Array();
  var arr_remark = new Array();

  for (let i = 0; i < arr_List_Bank.length; i++) {
    var str_user_name = new String();
    var str_location_name = new String();

    for (let k = 0; k < arr_users.length; k++) {
      if (String(arr_users[k].Id) == String(arr_List_Bank[i].AuthorId)) {
        str_user_name = arr_users[k].Title;
        num_remark = arr_List_Bank[i].Remark;
      }
    }
    arr_user_name.push(str_user_name);
    arr_remark.push(parseInt(num_remark));
  }

  // Set new default font family and font color to mimic Bootstrap's default styling
  Chart.defaults.global.defaultFontFamily =
    '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
  Chart.defaults.global.defaultFontColor = "#292b2c";

  // Bar Chart Example
  var ctx_1 = document.getElementById("myBarChart1");
  var ctx_2 = document.getElementById("myBarChart2");
  var ctx_3 = document.getElementById("myBarChart3");

  var myLineChart1 = new Chart(ctx_1, {
    type: "bar",
    data: {
      labels: arr_user_name,
      datasets: [
        {
          label: "Remark",
          backgroundColor: "rgba(2,117,216,1)",
          borderColor: "rgba(2,117,216,1)",
          data: arr_remark
        }
      ]
    },
    options: {
      scales: {
        xAxes: [
          {
            time: {
              unit: "User"
            },
            gridLines: {
              display: true
            },
            ticks: {
              // maxTicksLimit: 6
            }
          }
        ],
        yAxes: [
          {
            ticks: {
              min: 0,
              max: 100
              // maxTicksLimit: 5
            },
            gridLines: {
              display: true
            }
          }
        ]
      },
      legend: {
        display: false
      }
    }
  });

  var myLineChart2 = new Chart(ctx_2, {
    type: "bar",
    data: {
      labels: arr_user_name,
      datasets: [
        {
          label: "Remark",
          backgroundColor: "rgba(2,117,216,1)",
          borderColor: "rgba(2,117,216,1)",
          data: arr_remark
        }
      ]
    },
    options: {
      scales: {
        xAxes: [
          {
            time: {
              unit: "User"
            },
            gridLines: {
              display: true
            },
            ticks: {
              // maxTicksLimit: 6
            }
          }
        ],
        yAxes: [
          {
            ticks: {
              min: 0,
              max: 100
              // maxTicksLimit: 5
            },
            gridLines: {
              display: true
            }
          }
        ]
      },
      legend: {
        display: false
      }
    }
  });

  var myLineChart3 = new Chart(ctx_3, {
    type: "bar",
    data: {
      labels: arr_user_name,
      datasets: [
        {
          label: "Remark",
          backgroundColor: "rgba(2,117,216,1)",
          borderColor: "rgba(2,117,216,1)",
          data: arr_remark
        }
      ]
    },
    options: {
      scales: {
        xAxes: [
          {
            time: {
              unit: "User"
            },
            gridLines: {
              display: true
            },
            ticks: {
              // maxTicksLimit: 6
            }
          }
        ],
        yAxes: [
          {
            ticks: {
              min: 0,
              max: 100
              // maxTicksLimit: 5
            },
            gridLines: {
              display: true
            }
          }
        ]
      },
      legend: {
        display: false
      }
    }
  });
  
  $.when(
    myLineChart1,
    myLineChart2,
    myLineChart3,
  ).done(function() {
  
    count = 0;
    setTimeout(() => {
      count = 1;
      console.log("load");
    }, 2000);

    // if ((count = 1)) {
    //   $(".carousel").slick({
    //     infinite: true,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     arrows: false,
    //     //   fade: true,
    //     autoplay: true,
    //     autoplaySpeed: 3000,
    //     // prevArrow: '<button class="slide-arrow prev-arrow"></button>',
    //     // nextArrow: '<button class="slide-arrow next-arrow"></button>'
    //   });
    // }
  });
});

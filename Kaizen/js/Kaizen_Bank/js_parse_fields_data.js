// PARSE DATA SELECT FIELDS
function load_select(arr_Fiels, str_Field_Id) {
    $("#" + str_Field_Id + " .add_value").remove();
    var html = new Array();
    for (let i = 0; i < arr_Fiels.length; i++) {
        html.push("<option value='" + arr_Fiels[i].Title.trim().toLowerCase().split(' ').join('-') + "'>" + arr_Fiels[i].Title + "</option>");
    }
    $("#" + str_Field_Id).append(html.join(''));
}
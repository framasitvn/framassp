// FILTER BY SELECT
var filter_By_Select = function (table_id, column, input_id) {
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            if (settings.nTable.id !== `${table_id}`) {
                return true;
            } else {
                var option = data[column];
                var option_str = option
                    .trim()
                    .toLowerCase()
                    .split(' ')
                    .join('-');
                var option_selected = $(`#${input_id}`).val();

                // DRAW ALL DATA WHEN CHOOSE ALL
                if (option_selected === "all") {
                    return true
                }

                if (option_str === option_selected) {
                    return true;
                }

                return false;
            }
        }
    )
}

// FILTER BY RANGE
// var filter_By_Min_Max = function (column, min_input_id, max_input_id) {
//     $.fn.dataTable.ext.search.push(
//         function (settings, data, dataIndex) {
//             var min = parseInt($(`#${min_input_id}`).val(), 10);
//             var max = parseInt($(`#${max_input_id}`).val(), 10);

//             var data_column = parseFloat(data[column]) || 0; // use data for the age column

//             if ((isNaN(min) && isNaN(max)) ||
//                 (isNaN(min) && data_column <= max) ||
//                 (min <= data_column && isNaN(max)) ||
//                 (min <= data_column && data_column <= max)) {
//                 return true;
//             }
//             return false;
//         }
//     );
// };
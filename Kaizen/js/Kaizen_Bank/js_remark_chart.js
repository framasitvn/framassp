// GET USER NAME & REMARK DATA
// var arr_List_Bank = get_List_JSON("list_kaizen_bank")
// var arr_users = get_List_JSON("list_users")
var arr_List_Bank = get_List_Bank_Testing()
var arr_users = get_User()

var arr_user_name = new Array()
var arr_remark = new Array()

for (let i = 0; i < arr_List_Bank.length; i++) {
    var str_user_name = new String()
    var str_location_name = new String()

    for (let k = 0; k < arr_users.length; k++) {
        if (String(arr_users[k].Id) == String(arr_List_Bank[i].AuthorId)) {
            str_user_name = arr_users[k].Title
            num_remark = arr_List_Bank[i].Remark
        }
    }
    arr_user_name.push(str_user_name)
    arr_remark.push(parseInt(num_remark))
}

// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Bar Chart Example
var ctx = document.getElementById("myBarChart");
var myLineChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: arr_user_name,
        datasets: [{
            label: "Remark",
            backgroundColor: "rgba(2,117,216,1)",
            borderColor: "rgba(2,117,216,1)",
            data: arr_remark,
        }],
    },
    options: {
        scales: {
            xAxes: [{
                time: {
                    unit: 'User'
                },
                gridLines: {
                    display: true
                },
                ticks: {
                    // maxTicksLimit: 6
                }
            }],
            yAxes: [{
                ticks: {
                    min: 0,
                    max: 50,
                    // maxTicksLimit: 5
                },
                gridLines: {
                    display: true
                }
            }],
        },
        legend: {
            display: false
        }
    }
});
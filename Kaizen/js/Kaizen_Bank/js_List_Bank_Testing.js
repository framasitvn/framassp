var siteUrl = "http://portal.framas.com/sites/kaizen";

// GET LIST BANK
function get_List_Bank_Testing() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Bank_Testing')/items?$top=1000&$filter=kaizen_status eq 'Approved'",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST USER
function get_User() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/SiteUsers",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST LOCATIONS
function get_Locations() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Location')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST AREA
function get_Areas() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Area')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST DEPARTMENT
function get_Department() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Department')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST EQUIPMENTS
function get_Equipments() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Equipment')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST WORKPIECES
function get_Workpieces() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Workpiece')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST IMPLEMENTATION DEPARTMENTS
function get_Implementation_Departments() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Implementation_Department')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST OUTCOMES
function get_Outcomes() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Outcome')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// GET LIST TYPE OF KAIZEN
function get_Kaizen_Type() {
    var arr_total_result = [];
    $.ajax({
        url: siteUrl +
            "/_api/web/lists/getbytitle('List_Type_Kaizen')/items?$top=100",
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            var items = data.d.results;
            arr_total_result = items;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_total_result;
}

// FILTER DATATABLES
var filter_By_Select = function (table_id, column, input_id) {
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
            if (settings.nTable.id !== `${table_id}`) {
                return true;
            } else {
                var option = data[column];
                var option_str = option.trim().toLowerCase().split(' ').join('-');
                var option_selected = $(`#${input_id}`).val();

                if (option_str === option_selected) {
                    return true;
                }

                return false;
            }
        }
    )
}

// SELECT
function load_select(arr_Fiels, str_Field_Id) {
    $("#" + str_Field_Id + " .add_value").remove();
    var html = new Array();
    for (let i = 0; i < arr_Fiels.length; i++) {
        html.push("<option value='" + arr_Fiels[i].Title.trim().toLowerCase().split(' ').join('-') + "'>" + arr_Fiels[i].Title + "</option>");
    }
    $("#" + str_Field_Id).append(html.join(''));
}

$(document).ready(function () {
    // GET LIST OBJECT
    var arr_List_Bank = get_List_Bank_Testing()
    var arr_users = get_User()
    var arr_locations = get_Locations()
    var arr_areas = get_Areas()
    var arr_departments = get_Department()
    var arr_equipments = get_Equipments()
    var arr_workpieces = get_Workpieces()
    var arr_implementation_departments = get_Implementation_Departments()
    var arr_outcome = get_Outcomes()
    var arr_kaizen_type = get_Kaizen_Type()

    console.log(arr_workpieces)

    var table_body = new Array()
    for (let i = 0; i < arr_List_Bank.length; i++) {
        var str_user_name = new String()
        var str_location_name = new String()

        for (let k = 0; k < arr_users.length; k++) {
            if (String(arr_users[k].Id) == String(arr_List_Bank[i].AuthorId)) {
                str_user_name = arr_users[k].Title
            }
        }

        for (let j = 0; j < arr_List_Bank.length; j++) {
            if (String(arr_locations[j].Id) == String(arr_List_Bank[i].kaizen_locationId)) {
                str_location_name = arr_locations[j].Title
            }
        }

        table_body.push(`<tr>
            <td>${arr_List_Bank[i]["Title"]}</td>
            <td>${arr_List_Bank[i]["Modified"]}</td>
            <td>${arr_List_Bank[i]["kaizen_code"]}</td>
            <td>${str_location_name}</td>
            <td>${str_user_name}</td>
            <td>${arr_List_Bank[i]["kaizen_version"]}</td>
        </tr>`)
    }

    // LOAD KAIZEN DATATABLES HTML
    $('#dataTable_body').append(table_body.join(''));

    // LOAD LOCATIONS SELECTS
    load_select(arr_locations, "locations")

    var table = $('#dataTable').DataTable();

    // Event listener to the two range filtering inputs to redraw on input
    // $('#min, #max').keyup(function () {
    //     filter_By_Min_Max(3, "min", "max");
    //     table.draw();
    // });

    $('#locations').change(function () {
        filter_By_Select("dataTable", 3, "locations");
        table.draw();
    });
});
$(document).ready(function () {
    /* GET LIST OBJECT REST API */
    var arr_List_Bank = get_List_Kaizen_Submission();
    // var arr_List_Bank = get_List_Bank_Testing()
    var arr_users = get_User()
    var arr_locations = get_Locations()
    var arr_areas = get_Areas()
    var arr_equipments = get_Equipments()
    var arr_workpieces = get_Workpieces()
    var arr_implementation_departments = get_Implementation_Departments()
    var arr_outcome = get_Outcomes()
    var arr_kaizen_type = get_Kaizen_Type()

    console.log(arr_List_Bank)

    /* GET LIST OBJECT JSON FILES */
    // var arr_List_Bank = get_List_JSON("list_kaizen_bank")
    // var arr_users = get_List_JSON("list_users")
    // var arr_locations = get_List_JSON("list_locations")
    // var arr_areas = get_List_JSON("list_areas")
    // var arr_departments = get_List_JSON("list_departments")
    // var arr_equipments = get_List_JSON("list_equipments")
    // var arr_workpieces = get_List_JSON("list_workpiece")
    // var arr_implementation_departments = get_List_JSON("list_implementation_departments")
    // var arr_outcome = get_List_JSON("list_outcome")
    // var arr_kaizen_type = get_List_JSON("list_kaizen_type")

    /* LOAD KAIZEN DATATABLES HTML */
    var table_body = new Array()
    for (let i = 0; i < arr_List_Bank.length; i++) {
        var str_user_name = new String()
        var str_location_name = new String()

        // USERS
        for (let j = 0; j < arr_users.length; j++) {
            if (String(arr_users[j].Id) == String(arr_List_Bank[i].AuthorId)) {
                str_user_name = arr_users[j].Title
            }
        }

        // LOCATIONS
        for (let j = 0; j < arr_locations.length; j++) {
            if (String(arr_locations[j].Id) == String(arr_List_Bank[i].kaizen_locationId)) {
                str_location_name = arr_locations[j].Title
            }
        }

        // AREAS
        for (let j = 0; j < arr_areas.length; j++) {
            if (String(arr_areas[j].Id) == String(arr_List_Bank[i].kaizen_areId)) {
                str_area_name = arr_areas[j].Title
            }
        }

        // Equipments
        for (let j = 0; j < arr_equipments.length; j++) {
            if (String(arr_equipments[j].Id) == String(arr_List_Bank[i].kaizen_equipmentId)) {
                str_equipments_name = arr_equipments[j].Title
            }
        }

        // Workpieces
        for (let j = 0; j < arr_workpieces.length; j++) {
            if (String(arr_workpieces[j].Id) == String(arr_List_Bank[i].kaizen_workpieceId)) {
                str_workpieces_name = arr_workpieces[j].Title
            }
        }

        // Imple Department 
        for (let j = 0; j < arr_implementation_departments.length; j++) {
            if (String(arr_implementation_departments[j].Id) == String(arr_List_Bank[i].kaizen_ImpleDepartmentId)) {
                str_implementation_departments_name = arr_implementation_departments[j].Title
            }
        }

        // Outcome 
        for (let j = 0; j < arr_outcome.length; j++) {
            if (String(arr_outcome[j].Id) == String(arr_List_Bank[i].kaizen_outComeId)) {
                str_outcome_name = arr_outcome[j].Title
            }
        }

        // Kaizen Type 
        for (let j = 0; j < arr_kaizen_type.length; j++) {
            if (String(arr_kaizen_type[j].Id) == String(arr_List_Bank[i].kaizen_typeId)) {
                str_kaizen_type_name = arr_kaizen_type[j].Title
            }
        }

        table_body.push(`<tr>
            <td>${arr_List_Bank[i].Title}</td>
            <td>${str_user_name}</td>
            <td>${str_location_name}</td>
            <td>${arr_List_Bank[i].kaizen_code}</td>
            <td>${arr_List_Bank[i].kaizen_version}</td>
            <td>${arr_List_Bank[i].Remark}</td>
            <td>${str_area_name}</td>
            <td>${str_equipments_name}</td>
            <td>${str_workpieces_name}</td>
            <td>${str_implementation_departments_name}</td>
            <td>${str_outcome_name}</td>
            <td>${str_kaizen_type_name}</td>
        </tr>`)
    }

    $('#dataTable_body').append(table_body.join(''));

    // LOAD SELECT FIELD
    load_select(arr_locations, "locations") // LOAD LOCATIONS SELECTS
    load_select(arr_areas, "kaizen_area") // LOAD AREA SELECTS
    load_select(arr_equipments, "kaizen_equipment") // LOAD EQUIPMENT SELECTS
    load_select(arr_workpieces, "kaizen_workpiece") // LOAD WORKPIECE SELECTS
    load_select(arr_implementation_departments, "kaizen_implementation_departments") // LOAD IMPLEMENTATION DEPARTMENTS SELECTS
    load_select(arr_outcome, "kaizen_outcome") // LOAD OUTCOM SELECTS
    load_select(arr_kaizen_type, "kaizen_type") // LOAD KAIZEN TYPE SELECTS

    var table = $('#dataTable').DataTable({
        "columnDefs": [{
            "targets": [5], // Remark Column
            "visible": false,
            "searchable": true
        }, {
            "targets": [6], // Area Column
            "visible": false,
            "searchable": true
        }, {
            "targets": [7], // Equipment Column
            "visible": false,
            "searchable": true
        }, {
            "targets": [8], // Workpiece Column
            "visible": false,
            "searchable": true
        }, {
            "targets": [9], // Implementation Department Column
            "visible": false,
            "searchable": true
        }, {
            "targets": [10], // Outcome Column
            "visible": false,
            "searchable": true
        }, {
            "targets": [11], // Type Kaizen Column
            "visible": false,
            "searchable": true
        }]
    });

    // var table = $('#dataTable').DataTable()

    // Event listener to the two range filtering inputs to redraw on input
    // $('#min, #max').keyup(function () {
    //     filter_By_Min_Max(3, "min", "max");
    //     table.draw();
    // });

    // RESET FILTER
    $('#btn_kaizen_reset_filter').click(function () {
        $('#kaizen_filter .form-control').val('all')
        table.draw();
    });

    $('#locations').change(function () {
        filter_By_Select("dataTable", 2, "locations");
        table.draw();
    });

    $('#kaizen_area').change(function () {
        filter_By_Select("dataTable", 6, "kaizen_area");
        table.draw();
    });

    $('#kaizen_equipment').change(function () {
        filter_By_Select("dataTable", 7, "kaizen_equipment");
        table.draw();
    });

    $('#kaizen_workpiece').change(function () {
        filter_By_Select("dataTable", 8, "kaizen_workpiece");
        table.draw();
    });

    $('#kaizen_implementation_departments').change(function () {
        filter_By_Select("dataTable", 9, "kaizen_implementation_departments");
        table.draw();
    });

    $('#kaizen_outcome').change(function () {
        filter_By_Select("dataTable", 10, "kaizen_outcome");
        table.draw();
    });

    $('#kaizen_type').change(function () {
        filter_By_Select("dataTable", 11, "kaizen_type");
        table.draw();
    });
});
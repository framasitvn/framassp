//this file storage all url links and all global variables

//url
var url_site = 'http://portal.framas.com/sites/kaizen';

//url home site
var url_home_site = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Home%20Page%20Component/html_Home.html';

//url for submit form
var url_submit_header = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Submit%20Form%20Component/html_Submit_From_Header.html';
var url_submit_field = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Submit%20Form%20Component/html_Submit_Form_Input_Field.html';
var url_submit_submitter_team_cc = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Submit%20Form%20Component/html_Submit_Form_Submitter_Team_CC.html';
var url_submit_image_attachment = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Submit%20Form%20Component/html_Submit_Form_Image_Attachment.html';
var url_submit_submitter_comment = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Submit%20Form%20Component/html_Submit_form_Comment.html';
var url_submit_button = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Submit%20Form%20Component/html_Submit_Form_Button.html';

//url for resubmit form
var url_resubmit_header = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Resubmit%20Form%20Component/html_Resubmit_From_Header.html';
var url_resubmit_field = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Resubmit%20Form%20Component/html_Resubmit_Form_Input_Field.html';
var url_resubmit_submitter_team_cc = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Resubmit%20Form%20Component/html_Resubmit_Form_Submitter_Team_CC.html';
var url_resubmit_image_attachment = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Resubmit%20Form%20Component/html_Resubmit_Form_Image_Attachment.html';
var url_resubmit_submitter_comment = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Resubmit%20Form%20Component/html_Resubmit_Form_Comment.html';
var url_resubmit_button = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Resubmit%20Form%20Component/html_Resubmit_Form_Button.html';

//url for show detail form
var url_detail_header = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Show%20Detail%20Form%20Component/html_Detail_From_Header.html';
var url_detail_field = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Show%20Detail%20Form%20Component/html_Detail_Form_Input_Field.html';
var url_detail_submitter_team_cc = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Show%20Detail%20Form%20Component/html_Detail_Form_Submitter_Team_CC.html';
var url_detail_condition_solution = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Show%20Detail%20Form%20Component/html_Detail_Form_Condition_Solution.html';
var url_detail_image_attachment = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Show%20Detail%20Form%20Component/html_Detail_Form_Image_Attachment.html';
var url_detail_submitter_note = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Show%20Detail%20Form%20Component/html_Detail_Form_Note.html';
var url_detail_approvals = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Show%20Detail%20Form%20Component/html_Detail_Form_Approval.html';
var url_detail_button_group = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Show%20Detail%20Form%20Component/html_Detail_Form_Button_Group.html';

//url for approval form
var url_approval_task = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Approval%20Task/html_Approval_Task.html';

//url for team approval form
var url_team_approval_button_group = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Team%20Approval%20Component/html_Team_Approval_Button_Group.html';

//url for supervisor approval form
var url_supervisor_approval_button_group = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Supervisor%20Approval%20Component/html_Supervisor_Approval_Button_Group.html';

//url for lean form
var url_lean_task = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Lean%20Form%20Component/html_Lean_Task.html';
var url_lean_task_addition_committer = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Lean%20Form%20Component/html_Lean_Task_Addition_Committer.html';
var url_add_committer_button_group = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Lean%20Form%20Component/html_Add_Committer_Button_Group.html';
var url_conclusion_button_group = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Lean%20Form%20Component/html_Conclusion_Button_Group.html';

//url for committer form
var url_committer_approval_button_group = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Committer%20Approval%20Component/html_Committer_Approval_Button_Group.html';

//url for following form
var url_following = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Follow%20Saving/html_Follow_Saving.html';
var url_following_button_group = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Follow%20Saving/html_Follow_Saving_Button_Group.html';
var url_follow_saving_attachment_comment = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Follow%20Saving/html_Follow_Saving_Attachment_Comment.html';
var url_follow_saving_approval = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Follow%20Saving/html_Follow_Saving_Approval.html';
var url_follow_saving_approval_button_group = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Follow%20Saving/html_Follow_Saving_Approval_Button_Group.html';
var url_follow_saving_approval_attachment_comment = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Follow%20Saving/html_Follow_Saving_Approval_Attachment_Comment.html';
var url_follow_saving_approver_comment = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Follow%20Saving/html_Follow_Saving_Approver_Comment.html';
var url_lean_follow_saving_button_group = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Follow%20Saving/html_Lean_Follow_Saving_Button_Group.html';

//url for report
var url_report = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Report/html_Report.html';
var url_bank = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Report/html_Bank.html';

//url for export files
var url_export = 'http://portal.framas.com/sites/kaizen/SiteAssets/html/Export/html_export.html';

//variables
var userId = _spPageContextInfo.userId;
var str_listName_location = 'List_Location';
var str_listName_outcome = 'List_Outcome';
var str_listName_type = 'List_Type_Kaizen';
var str_listName_impleDepartment = 'List_Implementation_Department';
var str_listName_area = 'List_Area';
var str_listName_equipment = 'List_Equipment';
var str_listName_workpiece = 'List_Workpiece';
var str_listName_department = 'List_Department';
var str_listName_kaizenSubmit = 'List_Kaizen_Submission';
var str_listName_image = 'Image_Save';
var str_listName_attachment = 'Attachment';
var str_listName_userPermission = 'List_User_Permission';
var str_listName_kaizenApproval = 'List_Kaizen_Approval';
var str_listName_reasons = 'List_Reasons';
var str_listName_follow_saving = 'List_Follow_Saving';
var str_listName_follow_saving_approval = 'List_Follow_Saving_Approval';
var str_listName_follow_saving_attachment = 'Attachment_Follow_Saving';
var str_listName_approver_Attachment = 'Attachment_Approver';

var url_New_Submit_Site = 'http://portal.framas.com/sites/kaizen/SitePages/newsubmitform.aspx';
var url_Resubmit_Site = 'http://portal.framas.com/sites/kaizen/SitePages/resubmitform.aspx';
var url_Show_Detail_Site = 'http://portal.framas.com/sites/kaizen/SitePages/showsubmitdetail.aspx';
var url_Team_Approval_Site = 'http://portal.framas.com/sites/kaizen/SitePages/teamapprovalform.aspx';
var url_Supervisor_Approval_Site = 'http://portal.framas.com/sites/kaizen/SitePages/supervisorapprovalform.aspx';
var url_Lean_Add_Committer_Site = 'http://portal.framas.com/sites/kaizen/SitePages/leanaddcommitterform.aspx';
var url_Lean_Conclusion_Site = 'http://portal.framas.com/sites/kaizen/SitePages/leanconclusionform.aspx';
var url_Committer_Approval_Site = 'http://portal.framas.com/sites/kaizen/SitePages/committerapprovalform.aspx';
var url_Following_Site = 'http://portal.framas.com/sites/kaizen/SitePages/followingform.aspx';
var url_Follow_Saving_Approval_Site = 'http://portal.framas.com/sites/kaizen/SitePages/followsavingapprovalform.aspx';
var url_Lean_Follow_Saving = 'http://portal.framas.com/sites/kaizen/SitePages/leanfollowform.aspx';
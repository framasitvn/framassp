function get_List_Information(str_Url) {
    var arr_results = [];
    $.ajax({
        url: str_Url,
        type: "GET",
        async: false,
        headers: {
            "accept": "application/json;odata=verbose",
        },
        success: function (data) {
            if(data.d.results != null){
                arr_results = data.d.results;
            }else{
                arr_results = data.d;
            }
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });
    return arr_results;
}

function update_List_Item(str_Site_Url, str_List_Name, int_Item_Id, arr_Update_Data, bool_is_return) {
    var clientContext = new SP.ClientContext(str_Site_Url);
    var oList = clientContext.get_web().get_lists().getByTitle(str_List_Name);
    this.oListItem = oList.getItemById(int_Item_Id);
    for (let a = 0; a < arr_Update_Data.length; a++) {
        oListItem.set_item(arr_Update_Data[a].Field, arr_Update_Data[a].Value);
    }
    oListItem.update();
    clientContext.executeQueryAsync(function () {
        console.log(str_List_Name + "update success.");
        if(bool_is_return){
            window.location.href = str_Site_Url;
        }
    }, function (sender, args) {
        console.log(str_List_Name + "update fail. " + args.get_message() + '\n' + args.get_stackTrace());
    });
}
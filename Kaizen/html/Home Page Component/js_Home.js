//set button new submit
$("#btn_new_submit").click(function () {
    window.location.href = url_New_Submit_Site;
})

//load all submits
var html_owner = new Array();
var html_team = new Array();
var html_supervisor = new Array();
var html_lean_add_committer = new Array();
var html_committer = new Array();
var html_following = new Array();
var html_follow_saving_approval = new Array();
for (let a = 0; a < arr_Submissions.length; a++) {
    var str_Kaizen_Title = arr_Submissions[a].Title;
    var str_Kaizen_Code = arr_Submissions[a].kaizen_code;
    var str_Kaizen_Status = new String();
    if (arr_Submissions[a].kaizen_status == "On Progress") {
        if (arr_Submissions[a].kaizen_currently_status != null) {
            if (arr_Submissions[a].kaizen_conclusionStatus != null) {
                str_Kaizen_Status = arr_Submissions[a].kaizen_conclusionStatus;
            } else {
                str_Kaizen_Status = arr_Submissions[a].kaizen_currently_status;
            }
        }
    } else {
        str_Kaizen_Status = arr_Submissions[a].kaizen_status;
    }

    var str_Author_Name = new String();
    for (let b = 0; b < arr_All_Users.length; b++) {
        if (arr_Submissions[a].AuthorId == arr_All_Users[b].Id) {
            str_Author_Name = arr_All_Users[b].Title;
        }
    }
    var arr_Follow_Saving = get_List_Information(url_site + "/_api/web/lists/getbytitle('" + str_listName_follow_saving + "')/items?$filter=Kaizen_Id eq " + arr_Submissions[a].Id);
    if (userId == arr_Submissions[a].AuthorId) {
        //load own submits
        var str_Show_Detail_Url = url_Show_Detail_Site + "?$id=" + arr_Submissions[a].Id;
        html_owner.push("<tr>");
        html_owner.push("<td>" + str_Kaizen_Title + "</td>");
        html_owner.push("<td>" + str_Kaizen_Code + "</td>");
        html_owner.push("<td>" + str_Kaizen_Status + "</td>");
        html_owner.push("<td><a href='" + str_Show_Detail_Url + "'>Show Detail</a></td>");
        if (str_Kaizen_Status == "Rejected") {
            var str_Resubmit_Url = url_Resubmit_Site + "?$id=" + arr_Submissions[a].Id;
            html_owner.push("<td><a href='" + str_Resubmit_Url + "'>Resubmit</a></td>");
        } else if (str_Kaizen_Status == "Approved") {
            if (arr_Submissions[a].Remark <= 0) {
                if (arr_Follow_Saving.length > 0 && arr_Follow_Saving[0].Status == "Send Approval") {
                    html_owner.push("<td>Send Approval</td>");
                } else {
                    var str_Follow_Saving_Url = url_Following_Site + "?$id=" + arr_Submissions[a].Id;
                    html_owner.push("<td><a href='" + str_Follow_Saving_Url + "'>Update Cost/Hour Saving</a></td>");
                }
            } else {
                html_owner.push("<td></td>");
            }
        } else {
            html_owner.push("<td></td>");
        }
        html_owner.push("</tr>");
    }

    if (str_Kaizen_Status == "Team Approval") {
        if (arr_Submissions[a].kaizen_teamId != null && arr_Submissions[a].kaizen_teamId.results.includes(userId)) {
            var str_Team_Url = url_Team_Approval_Site + "?$id=" + arr_Submissions[a].Id;
            html_team.push("<tr>");
            html_team.push("<td>" + str_Kaizen_Title + "</td>");
            html_team.push("<td>" + str_Author_Name + "</td>");
            html_team.push("<td>" + str_Kaizen_Code + "</td>");
            html_team.push("<td>" + str_Kaizen_Status + "</td>");
            html_team.push("<td><a href='" + str_Team_Url + "'>Team Approval</a></td>");
            html_team.push("</tr>");
        }
    } else if (str_Kaizen_Status == "Supervisor Approval") {//load supervisor submits
        if (userId == arr_Submissions[a].kaizen_supervisorId) {
            var str_Supervisor_Url = url_Supervisor_Approval_Site + "?$id=" + arr_Submissions[a].Id;
            html_supervisor.push("<tr>");
            html_supervisor.push("<td>" + str_Kaizen_Title + "</td>");
            html_supervisor.push("<td>" + str_Author_Name + "</td>");
            html_supervisor.push("<td>" + str_Kaizen_Code + "</td>");
            html_supervisor.push("<td>" + str_Kaizen_Status + "</td>");
            html_supervisor.push("<td><a href='" + str_Supervisor_Url + "'>Supervisor Approval</a></td>");
            html_supervisor.push("</tr>");
        }
    } else if (str_Kaizen_Status == "Lean Add Committer") {//load supervisor submits
        for (let b = 0; b < arr_Department.length; b++) {
            if (arr_Department[b].Title == "Lean" && arr_Submissions[a].kaizen_locationId == arr_Department[b].LocationId && arr_Department[b].UserId != null && arr_Department[b].UserId.results.includes(userId)) {
                var str_Add_Committer_Url = url_Lean_Add_Committer_Site + "?$id=" + arr_Submissions[a].Id;
                html_lean_add_committer.push("<tr>");
                html_lean_add_committer.push("<td>" + str_Kaizen_Title + "</td>");
                html_lean_add_committer.push("<td>" + str_Author_Name + "</td>");
                html_lean_add_committer.push("<td>" + str_Kaizen_Code + "</td>");
                html_lean_add_committer.push("<td>" + str_Kaizen_Status + "</td>");
                html_lean_add_committer.push("<td></td>");
                html_lean_add_committer.push("<td><a href='" + str_Add_Committer_Url + "'>Add Committer</a></td>");
                html_lean_add_committer.push("</tr>");
            }
        }
    } else if (str_Kaizen_Status == "Lean Conclusion") {//load supervisor submits
        for (let b = 0; b < arr_Department.length; b++) {
            if (arr_Department[b].Title == "Lean" && arr_Department[b].LocationId == arr_Submissions[a].kaizen_locationId && arr_Department[b].UserId != null && arr_Department[b].UserId.results.includes(userId)) {
                var str_Lean_Conclusion_Url = url_Lean_Conclusion_Site + "?$id=" + arr_Submissions[a].Id;
                html_lean_add_committer.push("<tr>");
                html_lean_add_committer.push("<td>" + str_Kaizen_Title + "</td>");
                html_lean_add_committer.push("<td>" + str_Author_Name + "</td>");
                html_lean_add_committer.push("<td>" + str_Kaizen_Code + "</td>");
                html_lean_add_committer.push("<td>" + str_Kaizen_Status + "</td>");
                html_lean_add_committer.push("<td></td>");
                html_lean_add_committer.push("<td><a href='" + str_Lean_Conclusion_Url + "'>Lean Conclusion</a></td>");
                html_lean_add_committer.push("</tr>");
            }
        }
        var arr_Approvals = get_List_Information(url_site + "/_api/web/lists/getbytitle('" + str_listName_kaizenApproval + "')/items?$filter=Kaizen_Id eq " + arr_Submissions[a].Id);
        for (let b = 0; b < arr_Approvals.length; b++) {
            if (arr_Approvals[b].Team_x002f_Committer == "Committer" && arr_Approvals[b].ApproverId == userId) {
                if (arr_Approvals[b].Approval == null || arr_Approvals[b].Approval == "") {
                    var str_Committer_Url = url_Committer_Approval_Site + "?$id=" + arr_Submissions[a].Id;
                    html_committer.push("<tr>");
                    html_committer.push("<td>" + str_Kaizen_Title + "</td>");
                    html_committer.push("<td>" + str_Author_Name + "</td>");
                    html_committer.push("<td>" + str_Kaizen_Code + "</td>");
                    html_committer.push("<td>" + str_Kaizen_Status + "</td>");
                    html_committer.push("<td><a href='" + str_Committer_Url + "'>Committer Approval</a></td>");
                    html_committer.push("</tr>");
                }
            }
        }
    } else if (str_Kaizen_Status == "Approved") {
        if (arr_Submissions[a].Remark <= 0) {
            for (let b = 0; b < arr_Department.length; b++) {
                if (arr_Department[b].Title == "Lean" && arr_Department[b].LocationId == arr_Submissions[a].kaizen_locationId && arr_Department[b].UserId != null && arr_Department[b].UserId.results.includes(userId)) {
                    var str_Lean_Follow_Saving_Url = url_Lean_Follow_Saving + "?$id=" + arr_Submissions[a].Id;
                    var str_Follow_Status = "Pending";
                    if (arr_Follow_Saving.length > 0) {
                        str_Follow_Status = arr_Follow_Saving[0].Status;
                    }
                    html_lean_add_committer.push("<tr>");
                    html_lean_add_committer.push("<td>" + str_Kaizen_Title + "</td>");
                    html_lean_add_committer.push("<td>" + str_Author_Name + "</td>");
                    html_lean_add_committer.push("<td>" + str_Kaizen_Code + "</td>");
                    html_lean_add_committer.push("<td>" + str_Kaizen_Status + "</td>");
                    html_lean_add_committer.push("<td>" + str_Follow_Status + "</td>");
                    if (str_Follow_Status == "Send Approval") {
                        html_lean_add_committer.push("<td></td>");
                    } else {
                        html_lean_add_committer.push("<td><a href='" + str_Lean_Follow_Saving_Url + "'>Follow Cost/Hour Saving</a></td>");
                    }
                    html_lean_add_committer.push("</tr>");
                }
            }
        }
        var arr_Follow_Saving_Approval = get_List_Information(url_site + "/_api/web/lists/getbytitle('" + str_listName_follow_saving_approval + "')/items?$filter=Kaizen_Id eq " + arr_Submissions[a].Id);
        if (arr_Follow_Saving_Approval.length > 0) {
            for (let b = 0; b < arr_Follow_Saving_Approval.length; b++) {
                if (userId == arr_Follow_Saving_Approval[b].ApproverId && arr_Follow_Saving_Approval[b].Status == 'Pending') {
                    var str_Follow_Saving_Aprroval_Url = url_Follow_Saving_Approval_Site + "?$id=" + arr_Submissions[a].Id + "&$approval_id=" + arr_Follow_Saving_Approval[b].Id;
                    html_follow_saving_approval.push("<tr>");
                    html_follow_saving_approval.push("<td>" + str_Kaizen_Title + "</td>");
                    html_follow_saving_approval.push("<td>" + str_Kaizen_Code + "</td>");
                    html_follow_saving_approval.push("<td><a href='" + str_Follow_Saving_Aprroval_Url + "'>Approval Cost/Hour Saving</a></td>");
                    html_follow_saving_approval.push("</tr>");
                }
            }
        }
    }
}

//draw owner submit
$("#tbl_owner_submits tbody").append(html_owner.join(''));
$("#tbl_owner_submits").DataTable({
    "scrollY": "350px",
    "scrollCollapse": true,
    "paging": false,
    "info": false
});
$("#tbl_owner_submits_filter").addClass("pull_left");

//draw team submit
$("#tbl_team_submits tbody").append(html_team.join(''));
$("#tbl_team_submits").DataTable({
    "scrollY": "350px",
    "scrollCollapse": true,
    "paging": false,
    "info": false
});
$("#tbl_team_submits_filter").addClass("pull_left");

//draw supervisor submit
$("#tbl_supervisor_submits tbody").append(html_supervisor.join(''));
$("#tbl_supervisor_submits").DataTable({
    "scrollY": "350px",
    "scrollCollapse": true,
    "paging": false,
    "info": false
});
$("#tbl_supervisor_submits_filter").addClass("pull_left");

//draw committer submit
$("#tbl_committer_submits tbody").append(html_committer.join(''));
$("#tbl_committer_submits").DataTable({
    "scrollY": "350px",
    "scrollCollapse": true,
    "paging": false,
    "info": false
});
$("#tbl_committer_submits_filter").addClass("pull_left");

//draw lean add committer submit
$("#tbl_lean_add_committer_submits tbody").append(html_lean_add_committer.join(''));
$("#tbl_lean_add_committer_submits").DataTable({
    "scrollY": "350px",
    "scrollCollapse": true,
    "paging": false,
    "info": false
});
$("#tbl_lean_add_committer_submits_filter").addClass("pull_left");

$("#tbl_following_approval_submits tbody").append(html_follow_saving_approval.join(''));
$("#tbl_following_approval_submits").DataTable({
    "scrollY": "350px",
    "scrollCollapse": true,
    "paging": false,
    "info": false
});
$("#tbl_following_approval_submits_filter").addClass("pull_left");

//set event when click on tab title
$(".home_content .tab_title>div>p").click(function () {
    $(".home_content .tab_title>div>p").removeClass("active");
    $(".home_content .submits_wrapper").removeClass("active_content");
    $(this).addClass("active");
    var str_Tab_Value = $(this)[0].textContent;
    if (str_Tab_Value == "Owner") {
        $("#owner_wrapper").addClass("active_content");
    } else if (str_Tab_Value == "Team") {
        $("#team_wrapper").addClass("active_content");
    } else if (str_Tab_Value == "Supervisor") {
        $("#supervisor_wrapper").addClass("active_content");
    } else if (str_Tab_Value == "Committer") {
        $("#committer_wrapper").addClass("active_content");
    } else if (str_Tab_Value == "Lean") {
        $("#lean_add_committer_wrapper").addClass("active_content");
    } else if (str_Tab_Value == "Following") {
        $("#following_wrapper").addClass("active_content");
    } else if (str_Tab_Value == "Follow Saving Approval") {
        $("#following_approval_wrapper").addClass("active_content");
    }
});